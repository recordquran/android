package ai.rdi.recordquran

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.*
import androidx.core.app.ActivityCompat
import android.webkit.PermissionRequest
import android.webkit.WebChromeClient
import java.util.*

private const val REQUEST_RECORD_AUDIO_PERMISSION = 200
private const val REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 100

class MainActivity : AppCompatActivity() {

    private lateinit var webView: WebView

    private val URL = "https://recordquran.org"

    private var isAlreadyCreated = false

    private var permissionToSTORGEAccepted = false

    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToSTORGEAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!permissionToSTORGEAccepted) finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webView = findViewById(R.id.webview)

        //Record audio
        ActivityCompat.requestPermissions(
            this@MainActivity,
            permissions,
            REQUEST_RECORD_AUDIO_PERMISSION
        )

        val prefs = getSharedPreferences("prefs", MODE_PRIVATE)
        val firstStart = prefs.getBoolean("firstStart", true)

        var url_lang = URL

        if(firstStart)
        {
            if(Locale.getDefault().language.startsWith("ar"))
            {
                url_lang = "$url_lang?lang=ar"
            }
            else
            {
                url_lang = "$url_lang?lang=en"
            }

            val editor = prefs.edit()
            editor.putBoolean("firstStart", false)
            editor.apply()
        }

        webView.apply {
            // Configure related browser settings
            webView.settings.loadsImagesAutomatically = true
            webView.settings.javaScriptEnabled = true
            webView.getSettings().setUseWideViewPort(true)
            // Zoom out if the content width is greater than the width of the viewport
            webView.getSettings().setLoadWithOverviewMode(true)
            webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            // Configure the client to use when opening URLs
            webView.webViewClient = WebViewClient()
            webView.webChromeClient = WebChromeClient().apply {

            }
            webView.webChromeClient = object : WebChromeClient() {
                override fun onPermissionRequest(request: PermissionRequest) {
                    request.grant(request.resources)
                }

                override fun onShowFileChooser(
                    webView: WebView?,
                    filePathCallback: ValueCallback<Array<Uri>>?,
                    fileChooserParams: FileChooserParams?
                ): Boolean {
                    return super.onShowFileChooser(webView, filePathCallback, fileChooserParams)
                }

            }
            val webSettings = webView.settings
            webSettings.javaScriptEnabled = true

            webSettings.domStorageEnabled = true
            webSettings.setDatabaseEnabled(true);
            val databasePath: String =
                getApplicationContext().getDir("database", MODE_PRIVATE).getPath()
            settings.databasePath = databasePath
            webSettings.loadWithOverviewMode = true
            webSettings.useWideViewPort = true
            //
            webSettings.allowFileAccessFromFileURLs=true
            webSettings.allowFileAccess=true
            webSettings.allowUniversalAccessFromFileURLs=true
            //
            webSettings.builtInZoomControls = true
            webSettings.displayZoomControls = false
            webSettings.mediaPlaybackRequiresUserGesture=false
            webSettings.setSupportZoom(true)
            webSettings.defaultTextEncodingName = "utf-8"
            // Load the initial URL
            // ActivityCompat.requestPermissions(this@MainActivity, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

            webView.loadUrl(url_lang)
        }}}
